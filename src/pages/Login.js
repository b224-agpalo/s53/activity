import { useState, useEffect } from 'react';
import { Form, Button } from 'react-bootstrap';


export default function Login() {

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	// State to determine whether the submit button is enabled or not.
	const [isActive, setIsActive] = useState(false);



	// Function to simulate user registration
	function authenticate(e) {
		//e.preventDefault()


		localStorage.setItem("email", email);

		// Clear the input fields and states
		setEmail("");
		setPassword("");


		alert(`${email} has been verified! Welcome back!`);
	}


	useEffect(() => {
		if ((email !== "" && password !== "")) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);


	return (

		<Form onSubmit={e => authenticate(e)}>

			<h1 className="text-center my-3">Login</h1>

			<Form.Group className="mb-3" controlId="userEmail">
				<Form.Label>Email address</Form.Label>
				<Form.Control
					type="email"
					placeholder="Enter email"
					value={email}
					onChange={e => setEmail(e.target.value)}
					required
				/>
			</Form.Group>

			<Form.Group className="mb-3" controlId="password">
				<Form.Label>Password</Form.Label>
				<Form.Control
					type="password"
					placeholder="Password"
					value={password}
					onChange={e => setPassword(e.target.value)}
					required
				/>
			</Form.Group>

			{
				isActive ?
					<Button variant="primary" type="submit" id="submitBtn">
						Submit
					</Button>
					:
					<Button variant="primary" type="submit" id="submitBtn" disabled>
						Submit
					</Button>
			}

		</Form>

	)

}