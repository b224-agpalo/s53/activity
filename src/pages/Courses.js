import CourseCard from '../components/CourseCard';
import coursesData from '../data/coursesData';

export default function Courses() {

	// Checks to see if the mock data was captured.
	// console.log(coursesData);
	// console.log(coursesData[0]);

	const courses = coursesData.map(course => {
		return (
			<CourseCard key={course.id} courseProp={course} />
		)
	})


	return (
		<>
			<h1>Courses</h1>
			{courses}
		</>
	)
}