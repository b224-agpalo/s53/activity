
import { Link } from 'react-router-dom';
import { Row, Col, Button } from 'react-bootstrap';


export default function Error() {

    return (

        <Row>
            <Col className="p-5">
                <h1>Error 404- Page Not Found</h1>
                <p>This page you are looking for cannot be found</p>
                <Button variant="primary" as={Link} to="/">Back to Home</Button>
            </Col>
        </Row>
    )

}